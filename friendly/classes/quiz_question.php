<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates an object of type quiz_question to incorporate in the model for a report
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class quiz_question extends question {
	
	/* Instance variables and functions from parent class
	 *
	 * public $id;
	 * public $type;
	 * public $text;
	 * public $answer;
	 * public $correctAnswer;
	 * public $weighting;
	 * public $time;
	 * public $result;
	 *
	 * Overridden to remove unwanted question data (weight, type, etc)
	 * public function addProperty($name, $value)
	 *
	 * Returns a normalized correct string
	 * public function getCorrectText()
	 * 
	 * Overridden to remove quiz general data (time, score, etc)
	 * public function addQuizData($datum)
	*/

	public $questionIndex;
	public $userID;
	public $attemptNum;
	public $raw;

	/**
	 * Important! Set $index to false to retrieve all questions matching $attemptNum and $userId
 	 * Set $index to true or a positive number to retrieve a single question
 	 *
 	 * Builds a quiz_question object by traversing the $data and gathering field data related to the value of $questionIndex
 	 *
 	 * @param array $data - the quiz data to be processed
 	 * @param num $questionIndex - the index of the question if a single question is to be created
 	 * @param string $userID - the userid of the question(s) to be created
	 * @param string $attemptNum - the attempt number of the question(s) to be created
 	 * @param boolean $raw - whether the object will return one or many question objects
 	 */
	public function __construct($data, $questionIndex, $userID, $attemptNum, $raw) {
		
		unset($this->time);
		unset($this->weighting);
		//unset($this->type);
		$this->questionIndex = $questionIndex;
		$this->raw = $raw;

		// The question will have no attempt data
		if ($raw == true) {
			unset($this->userID);
			unset($this->attemptNum);
			unset($this->result);
			unset($this->answer);
			foreach ($data as $datum) {
				$this->addQuizData($datum);
			}
		// The question will have attempt data
		} else {
			$this->userID = $userID;
			$this->attemptNum = $attemptNum;
			foreach ($data as $datum) {
				$this->addQuizData($datum);
			}
		}
	}

	/**
     * Add a piece of data to the model
     *
     * @param string $datum - record from the $data array
     * @return none
     */
	public function addQuizData($datum) {

		// The data is filtered by userid and attempt
		if ($this->raw == false) {
			// If this is an interaction, add the question to the list.
			if (strstr($datum->element, 'cmi.interactions') && $datum->userid == $this->userID &&$datum->attempt == $this->attemptNum) {
				$this->addQuestion($datum->element, $datum->value);
			}
		// The data is not filtered by userid and attempt
		} else {
			// If this is an interaction, add the question to the list.
			if (strstr($datum->element, 'cmi.interactions')) {
				$this->addQuestion($datum->element, $datum->value);
			}
		}
	}

	/**
     * Add a question, creating a new one if it doesn't yet exist in the array
     * @param string $name - element field from the $data array
     * @param string $value - value field from the $data array
     * @return none
     */
	public function addQuestion($name, $value) {

		preg_match('/(?:cmi\.interactions)(?:[_\.]{1})([0-9]{1,})(?:\.)(.*)/', $name, $matches);
		$num = $matches[1];
		$name = $matches[2];
		if ($num == $this->questionIndex ) {
			$this->addProperty($name, $value);
		}
	}

	/**
     * Add a property to the question; some fields are conditionally removed for raw questions
     * @param string $name - element field from the $data array
     * @param string $value - value field from the $data array
     * @return none
     */
	// Add a property to the question; some fields are removed for raw questions
	public function addProperty($name, $value) {

		switch ($name) {
			case 'id':
				$this->id = $value;
				break;
			case 'correct_responses.0.pattern':
			case 'correct_responses_0.pattern':
				$this->correctAnswer = $value;
				break;
			case 'type':
				$this->type = $value;
				break;
			case 'result':
				if ($this->raw == false) {
					$this->result = $value;
				}
				break;
			case 'description':
				$this->text = $value;
				break;
			case 'learner_response':
			case 'student_response':
				if ($this->raw == false) {
					$this->answer = $value;
				}
				break;
			default:
				break;
		}
	}

	/**
     * Static method that parses $data array record element strings
     * @param string $element - element field from the $data array
     * @param string $value - value field from the $data array
     * @return array containing the parsed element's number and identifier
     */
	public static function parseElement($element) {

		preg_match('/(?:cmi\.interactions)(?:[_\.]{1})([0-9]{1,})(?:\.)(.*)/', $element, $matches);
		return $matches;
	}
}
