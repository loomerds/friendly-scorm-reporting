<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * User model for a report
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class userquestions {

	public $uniqueid;
	public $id;
	public $attempt;
	public $first;
	public $last;
	public $questions = [];
	public $time;
	public $score;
	public $scoreMax;
	public $scoreMin;
	public $status;
	public $email;

	/**
     * Get user and attempt information on init
     *
     * @param num $id - user id
     * @param num $attempt - attempt number
     */
	public function __construct($id, $attempt) {
		$this->id = $id;
		$this->attempt = $attempt;
		$this->uniqueid = $id . "#" . $attempt;
	}

	/**
     * Add a piece of data to the model
     * @param string $datum - record from the $data array
     * @return none
     */
	public function addQuizData($datum) {

		// If this is an interaction, add the question to the list.
		if (strstr($datum->element, 'cmi.interactions')) {
			return $this->addQuestion($datum->element, $datum->value);
		}
		
		// Handle meta quiz stuff
		switch($datum->element) {
			case 'cmi.total_time':
			case 'cmi.core.total_time':
				$this->time = $datum->value;
				break;
			case 'cmi.score.raw':
			case 'cmi.core.score.raw':
				$this->score = $datum->value;
				break;
			case 'cmi.core.score.max':
				$this->scoreMax = $datum->value;
				break;
			case 'cmi.core.score.min':
				$this->scoreMin = $datum->value;
				break;
			case 'cmi.completion_status':
			case 'cmi.core.lesson_status':
				$this->status = $datum->value;
				break;
		}
	}

	/**
     * Add a question, creating a new one if it doesn't yet exist in the array
     * @param string $name - element field from the $data array
     * @param string $value - value field from the $data array
     * @return none
     */
	public function addQuestion($name, $value) {

		preg_match('/(?:cmi\.interactions)(?:[_\.]{1})([0-9]{1,})(?:\.)(.*)/', $name, $matches);
		$num = $matches[1];
		$name = $matches[2];
		if (!isset($this->questions[$num])) {
			$this->questions[$num] = new question();
		}
		$this->questions[$num]->addProperty($name, $value);
	}

	/**
     * Sets firstname, lastname and email fields of a $user object
     * @param object $user - a user object
     * @return none
     */
	public function setUser($user) {

		$this->first = $user->firstname;
		$this->last = $user->lastname;
		$this->email = $user->email;
	}
}
