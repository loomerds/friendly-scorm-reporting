<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core table class for scoremreport_trends
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die;

class table extends \flexible_table {
    
    /**
     * Sets up the table parameters.
     *
     * @param string $uniqueid unique id of form.
     * @param null
     * @param string $tableType - user or question layouts
     */
    public function __construct($uniqueid, $data = null, $tableType) {

        parent::__construct($uniqueid);
        if ($tableType == 'userGrouped') {
            $this->configure_columns_users();
        } else {
            $this->configure_columns_questions();
        }
    }

    /**
     * Sets up the table columns for by-user view
     *
     * @param none
     * @return none
     */
    public function configure_columns_users() {
        
        global $PAGE;
        $columns = array('fullname', 'attempt', 'question', 'answer', 'correct', 'correct_answer');
        $headers = array(
            get_string('student', 'scormreport_friendly'),
            get_string('attempt', 'scormreport_friendly'),
            get_string('question', 'scormreport_friendly'),
            get_string('answer', 'scormreport_friendly'),
            get_string('correct', 'scormreport_friendly'),
            get_string('correct_answer', 'scormreport_friendly')
        );
        $this->define_columns($columns);
        $this->define_headers($headers);
        $this->define_baseurl($PAGE->url);
        // Don't show repeated data.
        $this->column_suppress('fullname');
        //$this->column_suppress('attempt'); // removed because it wasn't working properly - ideally we want it
        $this->collapsible(false); // should be true in v2?
        $this->sortable(true, 'firstname', SORT_ASC); // true in v2
        $this->pageable(true);
        $this->is_downloadable(false);
        $this->initialbars(true); // loads the filer table for v2
        
        //$this->set_attribute('class', 'friendly-form'); // this was commented out in v1

        // control what can be sorted in v2
        $this->no_sorting('attempt');
        $this->no_sorting('question');
        $this->no_sorting('answer');
        $this->no_sorting('correct');
        $this->no_sorting('correct_answer');

        $this->setup();
    }
    
    /**
     * This function is not used in v2
     * Sets up the table columns for by-qustion view
     *
     * @param none
     * @return none
     */

    public function configure_columns_questions() {
        
        global $PAGE;
        $columns = array('question', 'correct_answer', 'student', 'attempt', 'answer', 'correct');
        $headers = array(
            get_string('question', 'scormreport_friendly'),
            get_string('correct_answer', 'scormreport_friendly'),
            get_string('student', 'scormreport_friendly'),
            get_string('attempt', 'scormreport_friendly'),
            get_string('answer', 'scormreport_friendly'),
            get_string('correct', 'scormreport_friendly')
        );
        $this->define_columns($columns);
        $this->define_headers($headers);
        $this->define_baseurl($PAGE->url);
        // Don't show repeated data.
        //$this->column_suppress('student');
        //$this->column_suppress('attempt');
        $this->column_suppress('question');
        $this->column_suppress('correct_answer');
        $this->collapsible(false);
        $this->sortable(false);
        $this->pageable(false);
        $this->is_downloadable(false);
        // $this->set_attribute('class', 'friendly-form');
        $this->setup();
    }

    /**
     * Parses table data for display grouped by users
     *
     * @param array
     * @return none
     */
    public function add_users_data($users) {  
        foreach ($users as $user) {
            $name = $user->first . ' ' . $user->last . '<br><small><a href="mailto:' . $user->email . '">' . $user->email . '</a></small>';
            foreach ($user->questions as $question) {
                $this->add_data([$name, $user->attempt, $question->text ? $question->text : $this->get_readable_question_id($question->id), $question->answer, $question->getCorrectText(), $question->correctAnswer]);
            }
        }
    }

    /**
     * Get a readable version of a question ID.
     */
    public function get_readable_question_id($id) {
        return preg_replace('/[_]{1,}/', ' ', $id);
    }

    /**
     * Parses table data for display grouped by questions
     * Not used in v2
     * @param array
     * @return none
     */
    
    public function add_question_data($questions) {
        
        foreach ($questions as $question) {
            foreach ($question->groupedQuestions as $groupedQuestion) {
                    $qText = $groupedQuestion[0]->text ? $groupedQuestion[0]->text : $groupedQuestion[0]->id;
                    $cAnswer = $groupedQuestion[0]->correctAnswer;
                foreach ($groupedQuestion[1] as $quiz_user) {
                    $name = $quiz_user->first . ' ' . $quiz_user->last . '<br><small><a href="mailto:' . $quiz_user->email . '">' . $quiz_user->email . '</a></small>';
                    foreach ($quiz_user->attempts as $attempt) {
                        $this->add_data([$qText, $cAnswer, $name, $attempt->attemptNum, $attempt->questions[0]->answer, $attempt->questions[0]->getCorrectText()]);
                    }
                }   
            }
        }
    }
}
