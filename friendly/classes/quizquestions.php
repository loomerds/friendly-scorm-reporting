<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates a question model for a report
 * For the purposes of creating a question model (quizquestions object):
 *		a quizquestions HAS-A (raw) quiz_question and it HAS-A quiz_user array
 *		a quiz_user HAS-A a quiz_attempt array
 *		a quiz_attempt HAS-A quiz_question array
 *
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();


class quizquestions {

	public $groupedQuestions = [];

	/**
	 * Important! Set $index to false to retrieve all questions matching $attemptNum and $userId
 	 * Set $index to true or a positive number to retrieve a single question
 	 *
 	 * @param array $data - the quiz data to be processed
 	 * @param array $users - the user data to be processed
 	 * @param boolean $oneQuestion - whether the object will have one or many question objects
 	 */
	public function __construct($data, $users, $oneQuestion) {

		$indexCount = 0;
		$indexMax = 0;
		
		// Determine the number of questions in the quiz
		foreach ($data as $datum){
			$matches = quiz_question::parseElement($datum->element);

			if (isset($matches[2]) == 'id') {
				if ($matches[1] > $indexMax) {
					$indexMax = $matches[1];
				}
			}
		}
		$indexMax++;

		/*
		 * Depending upon the value of $oneQuestion, this creates an 
		 * object of nested arrays that contains all question responses 
		 * or only the responses that match the primary sort questions - 
		 * the latter is the desired behavior and the normal call to the 
		 * constructor of this class should pass true as a value for 
		 * $onQuestion
		 */
		while($indexCount < $indexMax) {
			$quizUsers = [];
			$rawQuestions = [];
			$rawQuestions[] = new quiz_question($data, $indexCount, 0, 0, true);
			foreach ($users as $user) {
				if ($oneQuestion == true) {
					$quizUsers[] = new quiz_user($data, $user->id, $user->firstname, $user->lastname, $user->email, $indexCount);
				} else {
					$quizUsers[] = new quiz_user($data, $user->id, $user->firstname, $user->lastname, $user->email, -1);
				}

			}
			$rawQuestions[] = $quizUsers;
			$this->groupedQuestions[] = $rawQuestions;
			$indexCount++;
		}
	}
}
