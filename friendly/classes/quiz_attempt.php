<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates an object of type quiz_attempt to incorporate in the model for a report
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class quiz_attempt {

	public $attemptNum;
	public $userID;
	public $questions = [];	

	/**
	 * Important! Set $index to false to retrieve all questions matching $attemptNum and $userId
 	 * Set $index to true or a positive number to retrieve a single question
 	 *
 	 * Builds a quiz_question object by traversing the $data and gathering field data related to the value of $questionIndex
 	 *
 	 * @param array $data - the quiz data to be processed
	 * @param string $attemptNum - the attempt number of the question(s) to be created
 	 * @param string $userID - the userid of the question(s) to be created
	 * @param string $attemptNum - the attempt number of the question(s) to be created
 	 * @param num $index - to be passed to quiz_attempt and ultimately to quiz_question 
 	 */
	public function __construct($data, $attemptNum, $userID, $index) {
		
		$this->attemptNum = $attemptNum;
		$this->userID = $userID;
		if ($index == -1) {
			$indexNum = 0;
		} else {
			$indexNum = $index;
		}

		// Build the questions array
		foreach ($data as $datum) {
			if (strstr($datum->element, 'cmi.interactions') && ($datum->userid == $this->userID) && ($datum->attempt == $this->attemptNum)) {
				if (quiz_question::parseElement($datum->element)[2] == 'id') {
					$this->questions[] = new quiz_question($data, $indexNum, $this->userID, $this->attemptNum, false);
					if ($index != -1) {
						break;
					}
					$indexNum = $indexNum + 1;
				}
			}	
		}
		
	}

}
