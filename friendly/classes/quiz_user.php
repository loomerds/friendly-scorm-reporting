<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Creates an object of type quiz_user to incorporate in the model for a report
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class quiz_user {

	public $userID;
	public $first;
	public $last;
	public $email;
	public $attempts = [];

	/** 
	 * Important! Set $index to -1 to retrieve all questions matching $attemptNum and $userId
	 * Set $index to 0 or a positive number to retrieve a single question
	 *
	 * @param array $data - the quiz data to be processed
 	 * @param string $userID - the user to be processed
 	 * @param string $first - the first name of the user to be processed
 	 * @param string $last - the last name of the user to be processed
 	 * @param string $email - the email of the user to be processed
 	 * @param num $index - to be passed to quiz_attempt and ultimately to quiz_question 
	 */
	public function __construct($data, $userID, $first, $last, $email, $index) {
		
		$this->userID = $userID;
		$this->first = $first;
		$this->last = $last;
		$this->email = $email;
		
		$attemptNum = 0;

		foreach ($data as $datum) {
			if ($datum->userid == $this->userID && $datum->attempt > $attemptNum) {
				$this->attempts[] = new quiz_attempt($data, ++$attemptNum, $this->userID, $index);
			}
		}
	}
}
