<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * * Creates an object of type question to incorporate in the model for a report
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class question {
	public $id;
	public $type;
	public $text;
	public $answer;
	public $correctAnswer;
	public $weighting;
	public $time;
	public $result;

	// Add a property to the question
	public function addProperty($name, $value) {
		switch ($name) {
			case 'id':
				$this->id = $value;
				break;
			case 'correct_responses.0.pattern':
			case 'correct_responses_0.pattern':
				$this->correctAnswer = $value;
				break;
			case 'type':
				$this->type = $value;
				break;
			case 'weighting':
				$this->weighting = $value;
				break;
			case 'result':
				$this->result = $value;
				break;
			case 'description':
				$this->text = $value;
				break;
			case 'learner_response':
			case 'student_response':
				$this->answer = $value;
				break;
		}
	}

	// Get a normalized correct string
	public function getCorrectText() {
		
		if ($this->type === 'true-false' || $this->type === 'choice' || $this->type === 'matching') {
			if ($this->result === 'wrong') return 'Incorrect';
			if ($this->result === 'correct') return 'Correct';
		}

		return '';
	}
}
