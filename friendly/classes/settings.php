<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the version of scorm_friendly
 * @package   scormreport
 * @subpackage friendly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");
class settings extends \moodleform {

    public function definition() {
        global $COURSE;
        $mform    =& $this->_form;

        /* In this v2 of the plugin we are removing the seldom used feature of Group by question.
         * By commenting out the following code, the Group by selection box and the Go submit button
         * will no longer appear on the report result page.
         */

        /*$mform->addElement('select', 'groupBy', get_string('group_by', 'scormreport_friendly'), [
            'user' => 'User',
            'question' => 'Question'
        ]);

        $this->add_action_buttons(false, get_string('go', 'scormreport_friendly'));*/
    }
}
