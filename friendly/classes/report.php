<?php
// This file is part of SCORM Friendly report plugin for Moodle
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Core Report class of basic reporting plugin
 * @package   scormreport
 * @subpackage frieldly
 * @author    D Loomer
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace scormreport_friendly;
defined('MOODLE_INTERNAL') || die();

class report extends \mod_scorm\report {
    
    public $table;
    public $form;
    private $sort;
    private $tifirst;
    private $tilast;
    private $baseurl;

    /**
     * displays the full report
     * @param \stdClass $scorm full SCORM object
     * @param \stdClass $cm - full course_module object
     * @param \stdClass $course - full course object
     * @param string $download - type of download being requested
     */
    public function display($scorm, $cm, $course, $download) {
        
        global $PAGE;
        global $OUTPUT;
        global $USER;

        // Import form objects
        $this->downloadForm = new download_form();
        $this->settingsForm = new settings_form($PAGE->url, compact('currentgroup'));

        // Set the "Page size" option on the settingsForm
        if ($fromform = $this->settingsForm->get_data()) {
            $pagesize = $fromform->pagesize;
            set_user_preference('scorm_report_pagesize', $pagesize);
        } else {
            $pagesize = get_user_preferences('scorm_report_pagesize', 0);
        }
        if ($pagesize < 1) {
            $pagesize = SCORM_REPORT_DEFAULT_PAGE_SIZE;
        }
        
        $this->settingsForm->set_data(array('pagesize' => $pagesize));

        /* In this v2 update to the plugin, the group by question option is being removed
         * to simplify the user interface.
         * To that end the following code block is commented out and the $groupBy variable
         * is being hard-coded to 'user'.
         */

        // Get data from the settings
        /*if ($fromform = $this->settingsForm->get_data()) {
            $groupBy = $fromform->groupBy;
            set_user_preference('scorm_report_friendly_groupBy', 'user');
        }
        else {
            $groupBy = get_user_preferences('scorm_report_friendly_groupBy');
        }*/

        // Get raw quiz data and create the create the dataset to be added to the form 
        $groupBy = 'user'; // added in v2

        if ($groupBy === 'user') {
            $tableType = 'userGrouped';
            // Create a new table instance for the report
            $this->table = new table('mod-scorm-friendly-report-' . $scorm->id, null, $tableType);
            $this->setIFilters();
            $data = $this->getQuizDBRows($scorm->id, $cm->id);
            $groupedData = $this->groupQuizDataByUser($data);

        } else {
            $tableType = 'questionGrouped';
            $groupedData = $this->groupQuizDataByQuestion($data);
        }
        
        // Count the total number of users who have made attempts
        $attemptsKeys = array_keys($groupedData);
        $attemptsUserNums = [];

        foreach ($attemptsKeys as $value) {
            $attemptsUserNums[] = substr($value, 0, 1);
        }

        $totalcount = count(array_unique($attemptsUserNums));

        // Filter groupedData for individual page
        if($totalcount > $pagesize) {
            $groupedData = $this->getPageRecords($groupedData, $pagesize, $page);
        }

        // No data, don't do anything.
        if (!count($groupedData)) {
            $this->table->print_nothing_to_display();
            $this->tifirst = optional_param('tifirst', 'all', PARAM_NOTAGS);
            $this->tilast = optional_param('tilast', 'all', PARAM_NOTAGS);
            set_user_preference('ifirst', null);
            set_user_preference('ilast', null);
            return;
        }

        // A download was requested
        if ($download) {
            $downloader = new downloader($groupedData);
            $downloader->run($download, $groupBy, clean_filename("$course->shortname ".format_string($scorm->name, true, array('context' => \context_course::instance($course->id)))));
        }
        // Finish rendering the page
        else {
            // Build and display the top paging bar
            $page=optional_param('page',0,PARAM_INT);
            $this->baseurl = $PAGE->url;
            echo $OUTPUT->paging_bar($totalcount, $page, $pagesize, $this->baseurl);
   
            // Add data to the table for the proper grouping
            if ($groupBy === 'user') {
                $this->table->add_users_data($groupedData);
            }
            else {
                $this->table->add_question_data($groupedData);
            }

            // Render the table
            $this->table->finish_output();

            // Build and render the bottom paging bar
            $page=optional_param('page',0,PARAM_INT);
            $this->baseurl = $PAGE->url;
            echo $OUTPUT->paging_bar($totalcount, $page, $pagesize, $this->baseurl);

            // Render the download and preferences forms
            $this->downloadForm->display();
            $this->settingsForm->display();
        }
    }

    /**
     * Filter records for page to be displayed.
     * @param  array $groupedData - data to be filtered
     * @param int $pageSize - number of users to display on page
     * @param int $targetPage - number of page to be displayed
     * @return array
     */
    protected function getPageRecords($groupedData, $pageSize, $targetPage) {

        $viewedUserId = -1;     // tracks id of last object
        $userCount = 0;         // tracks number of users for page
        $currentPageNum = 0;    // tracks number of pages traversed
        $pageRecords = [];      // array to hold target user objects

        foreach($groupedData as $record) {

            if($viewedUserId != $record->id) {
                $viewedUserId = $record->id;
                $userCount++;
            }
            if($userCount > $pageSize) {
                $currentPageNum++;
                $userCount=1;
            }
            if($currentPageNum == $targetPage) {
                $pageRecords[] = $record;
            }
            if($currentPageNum > $targetPage) {
                return $pageRecords;
            }
        }
        return $pageRecords;
    }

    /**
     * Take raw SQL data and group it per user.
     * @param  array
     * @return array
     */
    protected function groupQuizDataByUser($data) {

        // The grouped data to return
        $groups = [];

        // The user ids we need to load info on
        $userIds = [];

        // Build an array of key values by concatenating each record's user id and attempt #
        foreach ($data as $datum) {
            $key = $datum->userid . '#' . $datum->attempt;
            if (!isset($groups[$key])) {

                $groups[$key] = new userquestions ($datum->userid, $datum->attempt);
                
                //get an array of simple {user} id values to pass to getUsers() 
                $userIds[] = $datum->userid;
            }
            $groups[$key]->addQuizData($datum);
        }
        // Get the users we need
        $users = $this->getUsers($userIds);
        // Strip records filtered by initials bar from $groups
        $groups = array_intersect_key($groups, $users);

        // Map the user data back into the questions
        foreach ($groups as $group) {
            $group->setUser($users[$group->id . '#' . $group->attempt]);    
        }
        return $groups;
    }

    /**
     * Take raw SQL data and group it per question.
     * @param  array
     * @return array
     * Not used in v2
     */
    protected function groupQuizDataByQuestion($data) {

        // The grouped data to return
        $groups = [];

        // The user ids we need to load info on
        $userIds = $this->getUserIds($data);

        // Get the sql user data
        $users = $this->getUsers($userIds);

        // Create the quizquestion object
        $groups[] = new quizquestions($data, $users, true);
        
        return $groups;
    }

    /**
     * Get the raw DB data in firstname/lastname sorted order.
     * @param  int
     * @return array
     */

    protected function getQuizDBRows($scormId) {
        global $DB;
        if (!$scormId || !count($scormId)) return [];

        // Build sql query
        $sort = $this->getSortOrder();
        $select = 'SELECT DISTINCT '.$DB->sql_concat('st.id', '\'#\'', 'COALESCE(st.attempt, 0)').' AS uniqueid, ';
        $select .= 'st.id, st.userid, st.scormid, st.scoid, st.attempt, st.element, st.value, st.timemodified, u.firstname, u.lastname ';
        $from = 'FROM mdl_scorm_scoes_track AS st ';
        $from .= 'LEFT JOIN mdl_user AS u ON st.userid = u.id ';
        $where = 'WHERE st.scormid=' . $scormId;
        return $DB->get_records_sql($select.$from.$where.$sort);
    }

    /**
     * Get users for each given ID.
     * @param array
     * @return array
     */
    protected function getUsers($ids) {
        global $DB;
        if (!$ids || !count($ids)) return [];  
        
        // Build sql query
        $select = 'SELECT DISTINCT '.$DB->sql_concat('u.id', '\'#\'', 'COALESCE(st.attempt, 0)').' AS uniqueid, ';
        $select .= 'u.id, u.firstname, u.lastname, u.username, u.email ';
        $from = 'FROM mdl_user AS u ';
        $from .= 'LEFT JOIN mdl_scorm_scoes_track AS st ON st.userid = u.id ';
        $where = 'WHERE u.id in (' . implode(',', $ids) . ')';        
        $records = $DB->get_records_sql($select.$from.$where);
        $records = $this->filterQueryResultsByInitials($records);
        return $records;
    }

    /**
     * Filter sql query results by initial bar.
     * @param Array
     * @return Array
     */
    protected function filterQueryResultsByInitials($records) {

        // Filter if both a firstname and a lastname initial have been selected
        if (($this->tifirst != 'all' && $this->tifirst != null) && ($this->tilast != 'all' && $this->tilast != null)) {
            foreach ($records as $key => &$record) {

                if ($this->tifirst != strtoupper(substr($record->firstname,0,1)) || $this->tilast != strtoupper(substr($record->lastname,0,1))) {
                    unset($records[$key]);
                }
            }
            return $records;
        }
        // Filter by firstname first initial only
        if ($this->tifirst != 'all' && $this->tifirst != null) {
            foreach ($records as $key => &$record) {

                if ($this->tifirst != strtoupper(substr($record->firstname,0,1))) {
                    unset($records[$key]);
                }
            }
            return $records;
        }
        // Filter if both a firstname and a lastname initial have been selected
        if ($this->tilast != 'all' && $this->tilast != null) {
            foreach ($records as $key => &$record) {

                if ($this->tilast != strtoupper(substr($record->lastname,0,1))) {
                    unset($records[$key]);
                }
            }
            return $records;
        }
        return $records;
    }

    /**
     * Get sort order from table.
     * @return String
     */
    protected function getSortOrder() {

        // get the sort ORDER BY value from the table
        $tableSortOrder = $this->table->get_sql_sort();   

        // Provide for a default table sort or sort by column sort selctions made
        if (empty($tableSortOrder)) {
            $tableSortOrder = ' ORDER BY firstname ASC, lastname ASC';
        }else {
            $tableSortOrder = ' ORDER BY '.$tableSortOrder;
        }
        return $tableSortOrder;
    }

    /**
     * Set variables to track the initial bars filter choices.
     * @return none
     */

    protected function setIFilters() {
        global $USER;

        $this->tifirst = optional_param('tifirst', 'all', PARAM_NOTAGS);
        $this->tilast = optional_param('tilast', 'all', PARAM_NOTAGS);

        if (optional_param('treset', null , PARAM_INT)===1) {
            set_user_preference('ifirst', null);
            set_user_preference('ilast', null);
        }

        if ($this->tifirst !== 'all') {
            set_user_preference('ifirst', $this->tifirst);

        }
        if ($this->tilast !== 'all') {
            set_user_preference('ilast', $this->tilast);
        }
        
        if (!empty($USER->preference['ifirst'])) {
            $this->tifirst = $USER->preference['ifirst'];
        } else {
            $this->tifirst = 'all';
        }
        
        if (!empty($USER->preference['ilast'])) {
            $this->tilast = $USER->preference['ilast'];
        } else {
            $this->tilast = 'all';
        }
    }

    /**
     * Get an array of userids for students with attempts on this scorm exam
     * @param array
     * @return array of userids
     */
    protected function getUserIds($data) {
        foreach ($data as $datum) {
            $userIds[] = $datum->userid;
        }
        return $userIds;
    }
}
